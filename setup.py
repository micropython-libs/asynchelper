#!/usr/bin/env python

from distutils.core import setup

setup(name='asynchelper',
      version='0.1.0',
      description='Helper class that can run function in different thread and await result',
      author='Jan Klusáček',
      author_email='honza.klugmail.com',
      url='https://gitlab.com/users/honza.klu',
      packages=['asynchelper'],
     )