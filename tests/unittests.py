import time
import asyncio
import logging

import unittest

import asynchelper

logger = logging.getLogger(__name__)


def slow_function(duration: float):
    start_time = time.ticks_us()/1_000_000
    x = 0
    while time.ticks_us() / 1_000_000 - start_time < duration:
        x += 1
    return x


async def latency_test(duration: float):
    start_time = time.ticks_us()/1_000_000
    last_time = time.ticks_us()/1_000_000
    max_latency = 0
    while time.ticks_us()/1_000_000 - start_time < duration:
        latency = time.ticks_us()/1_000_000 - last_time
        if latency>max_latency:
            max_latency = latency
        await asyncio.sleep_ms(100)
        last_time = time.ticks_us()/1_000_000
    return max_latency


shared_var = None


def slow_func_shared_state(duration, value):
    global shared_var
    shared_var = value
    time.sleep(duration)
    return shared_var


def slow_func_with_params(*args, **kwargs):
    time.sleep(1)
    return args, kwargs


class TestSubtest(unittest.TestCase):
    def test_factory(self):
        helper = asynchelper.getAyncThreadHelper()
        assert helper == asynchelper.getAyncThreadHelper()

    def test_simple_call(self):
        loop = asyncio.get_event_loop()
        helper = asynchelper.getAyncThreadHelper()

        synchronous_func_task = loop.create_task(helper.await_function(slow_function, [10]))
        latency_test_task = loop.create_task(latency_test(10))

        start = time.ticks_us() / 1_000_000
        iterations, max_latency = asyncio.run(asyncio.gather(synchronous_func_task, latency_test_task))
        duration = time.ticks_us() / 1_000_000 - start

        self.assertLessEqual(max_latency, 0.010)
        self.assertLessEqual(duration, 11)
        logger.info(f"Duration: {duration} Max latency: {max_latency} Iterations: {iterations}")

    def test_concurrent_calls_run_in_executor(self):
        loop = asyncio.get_event_loop()
        helper = asynchelper.getAyncThreadHelper()

        slow_func_shared_state

        latency_test_task = loop.create_task(latency_test(10))
        slow_func_task = loop.create_task(
            helper.run_in_executor(slow_func_with_params, 1, 2, 3, param="param_str"))

        start = time.ticks_us() / 1_000_000
        max_latency, ret = asyncio.run(asyncio.gather(latency_test_task, slow_func_task))
        duration = time.ticks_us() / 1_000_000 - start

        logger.info(f"Duration: {duration} Max latency: {max_latency}")
        self.assertEqual(ret[0], (1, 2, 3))
        self.assertEqual(ret[1], {"param": "param_str"})

    def test_parallel_calls_timing(self):
        """ Check that synchronous calls are handled one after another

        """
        loop = asyncio.get_event_loop()
        helper = asynchelper.getAyncThreadHelper()

        # Run 10 synchronous tasks
        synchronous_func_tasks = [loop.create_task(helper.await_function(slow_function, [1])) for i in range(10)]
        latency_test_task = loop.create_task(latency_test(10))

        start = time.ticks_us()/1_000_000
        max_latency, *iterations = asyncio.run(asyncio.gather(latency_test_task, *synchronous_func_tasks))
        duration = time.ticks_us() / 1_000_000 - start

        logger.info(f"Duration: {duration} Max latency: {max_latency} Iterations: {iterations}")
        self.assertLessEqual(max_latency, 0.020)
        self.assertGreaterEqual(duration, 9)
        self.assertLessEqual(duration, 11)

    def test_concurrent_calls(self):
        loop = asyncio.get_event_loop()
        helper = asynchelper.getAyncThreadHelper()

        latency_test_task = loop.create_task(latency_test(10))
        concurrent_func_tasks = [loop.create_task(helper.await_function(slow_func_shared_state, [i / 100, i])) for i in
                                range(100)]

        start = time.ticks_us() / 1_000_000
        max_latency, *iterations = asyncio.run(asyncio.gather(latency_test_task, *concurrent_func_tasks))
        duration = time.ticks_us() / 1_000_000 - start

        logger.info(f"Duration: {duration} Max latency: {max_latency}")
        self.assertEqual(iterations, list(range(100)))

    def test_executor_calls(self):
        loop = asyncio.get_event_loop()
        helper = asynchelper.getAyncThreadHelper()

        latency_test_task = loop.create_task(latency_test(10))
        concurrent_func_tasks = [loop.create_task(helper.run_in_executor(slow_func_shared_state, i / 100, i)) for i in
                                range(100)]

        start = time.ticks_us() / 1_000_000
        max_latency, *iterations = asyncio.run(asyncio.gather(latency_test_task, *concurrent_func_tasks))
        duration = time.ticks_us() / 1_000_000 - start

        logger.info(f"Duration: {duration} Max latency: {max_latency}")
        self.assertEqual(iterations, list(range(100)))