# asynchelper

Micropython library that allows to run function in different thread and await for result

    athread_helper = asynchelper.getAyncThreadHelper()
    await athread_helper.await_function(self.connect)