import time
import _thread as thread
import uasyncio as asyncio

import logging
logger = logging.getLogger(__name__)

THREAD_WAIT_INTERVAL = 0.1 # [s]

helper = None

def getAyncThreadHelper() -> "AsyncThreadCall":
    global helper
    if helper is None:
        helper = AsyncThreadCall()
    return helper

class AsyncThreadCall:
    def __init__(self):
        self.helper_thread = None
        self.helper_lock = None
        self.helper_func = None
        self.helper_args = None
        self.helper_kwargs = None
        self.helper_ret = None
        self.async_lock = asyncio.Lock()
        self.start_helper()
        self.waiting_cnt = 0

    def start_helper(self):
        if self.helper_thread is None:
            logger.debug(f"start_helper")
            self.helper_lock = thread.allocate_lock()
            self.helper_lock.acquire() # This lock is started to prevent worker thread running before data is prepared
            self.helper_thread = thread.start_new_thread(self.helper_loop, ())
            logger.debug(f"start_helper started")

    def helper_loop(self):
        while True:
            if self.helper_func is None: # Prevent infinite function call when await_function is blocked
                time.sleep(0.001)
                continue
            logger.debug(f"Waiting for function")
            with self.helper_lock:
                logger.debug(f"Function {self.helper_func}(*{self.helper_args}, **{self.helper_kwargs}) called")
                self.helper_ret = self.helper_func(*self.helper_args, **self.helper_kwargs)
                logger.debug(f"Function returned {self.helper_ret} releasing lock")
                self.helper_func = None
                self.helper_args = None
                self.helper_kwargs = None

    async def await_function(self, func, args=None, kwargs=None):
        if args is None:
            args = ()
        if kwargs is None:
            kwargs = {}
        self.waiting_cnt += 1
        logger.debug(f'Called for function {getattr(func, "__name__", None)}(*{args},**{kwargs}).'
                     f'There are {self.waiting_cnt} calls waiting'
                     )
        async with self.async_lock:
            self.waiting_cnt -= 1
            self.helper_func = func
            self.helper_args = args
            self.helper_kwargs = kwargs
            self.helper_lock.release()
            await asyncio.sleep(THREAD_WAIT_INTERVAL)
            logger.debug(f"Waiting for function to return")
            while True:
                locked = self.helper_lock.acquire(False, 0.001)
                if locked:
                    break
                await asyncio.sleep_ms(10) # There is no async way to wait for thread.Lock
            logger.debug(f"Function returned")
            ret = self.helper_ret
            self.helper_ret = None
            return ret

    async def run_in_executor(self, func, *args, **kwargs):
        return await self.await_function(func, args, kwargs)
